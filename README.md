# terraform-vpc

This repo contains the terraform code of the VPC

### Manual Process ###
1) Create VPC
2) Create Subnets as per your range and choice
3) Create IGW
4) Attach IGW to VPC
5) Create Public RT and add the internet route as 0.0.0.0/0 over IGW
6) Create  NAT GW and add it to Public Subnet that has internet
7) Now create private-rt and add the route 0.0.0.0/0 as NGW
8) This whole action should give access to internet for public and private instances


# Manual Tasks that we are doing
1) We are manually adding peerting route in the default vpc's subnet route table  ( can this be automated?  This Route table is created not with Terrform)

# Private Machines cannot be access from internet, but private machine can talk to internet using NAT