bucket         = "batch48-tf-remotestate"
key            = "student/vpc/dev/terraform.tfstate"
region         = "us-east-1"
dynamodb_table = "terraform-locking"