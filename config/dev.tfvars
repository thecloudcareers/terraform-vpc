VPC_CIDR           = "10.15.0.0/16"
PROJECTNAME        = "student"
ENV                = "dev" 
PUBLIC_SUBNET_CIDR = ["10.15.0.0/24", "10.15.1.0/24"]
PRIVATE_SUBNET_CIDR = ["10.15.2.0/24", "10.15.3.0/24"]
DEFAULT_VPC_ID      = "vpc-0dd269d12252e793e"