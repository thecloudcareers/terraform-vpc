VPC_CIDR           = "10.17.0.0/16"
PROJECTNAME        = "student"
ENV                = "prod" 
PUBLIC_SUBNET_CIDR =  ["10.17.0.0/24", "10.17.1.0/24"]
PRIVATE_SUBNET_CIDR = ["10.17.2.0/24" , "10.17.3.0/24"]