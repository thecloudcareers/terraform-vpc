resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${var.PROJECTNAME}-${var.ENV}-igw"
  }
}


# Creates Elastic IP
resource "aws_eip" "nat" {
  vpc      = true
}

resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.nat.id 
  subnet_id     = aws_subnet.public.*.id[0] 

  tags = {
    Name = "${var.PROJECTNAME}-${var.ENV}-ngw"
  }

  depends_on = [aws_internet_gateway.igw]
}