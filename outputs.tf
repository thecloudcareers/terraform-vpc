output "vpc-op" {
    value = data.aws_vpc.default
}

output "VPC_ID" {
    value = aws_vpc.main.id
}

output "VPC_CIDR" {
    value = var.VPC_CIDR
}

output "DEFAULT_VPC_CIDR" {
 value = data.aws_vpc.default.cidr_block
}

output "PRIVATE_SUBNETS" {
    value = aws_subnet.private.*.id 
}

output "PUBLIC_SUBNETS" {
    value = aws_subnet.public.*.id 
}